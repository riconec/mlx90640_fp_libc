#ifndef MLX90640_FP_LIB
#define MLX90640_FP_LIB

#include <stdint.h>

/** 0x400 - 0x6FF */
typedef struct {
    uint16_t pixelsArray[768u];
} Mlx90640_pixelData_t;

/** 0x700, 0x708, 0x70A, 0x720, 0x728, 0x72A */
typedef struct {

} Mlx90640_pixelAux_t;

/** 0x2400 - 0x240F
 *  Only C, D, E and F are user writable */
typedef struct {

} Mlx90640_eepromControl_t;

/** 0x2410 */
typedef struct {
    uint16_t alphaPtat : 4;
    uint16_t scaleOccRow : 4;
    uint16_t scaleOccCol : 4;
    uint16_t scaleOccRem : 4;
} Mlx90640_eepromPtatOccScale_t;

/** 0x2420 */
typedef struct {
    uint16_t alphaScale : 4;
    uint16_t scaleAccRow : 4;
    uint16_t scaleAccCol : 4;
    uint16_t scaleAccRem : 4;
} Mlx90640_eepromAccScale_t;

/** 0x2432 */
typedef struct {
    int16_t vdd25 : 8;
    int16_t kvVdd : 8;
} Mlx90640_eepromVdd_t;

/** 0x2433 */
typedef struct {
    int16_t kvPtat : 10;
    int16_t ktPtat : 6;
} Mlx90640_eepromKPtat_t;

/** 0x2434 */
typedef struct {
    int16_t rowEvenColumnEven : 4;
    int16_t rowOddColumnEven  : 4;
    int16_t rowEvenColumnOdd  : 4;
    int16_t rowOddColumnOdd : 4;
} Mlx90640_eepromKvAvgRowCol_t;

/** 0x2435 */
typedef struct {
    int16_t ilChessC1 : 6;
    int16_t ilChessC2 : 5;
    int16_t ilChessC3 : 5;
} Mlx90640_eepromIlChess_t;

/** 0x2436 */
typedef struct {
    int16_t rowEvenColumnOdd : 8;
    int16_t rowEvenColumnOdd : 8;
} Mlx90640_eepromVdd_t;

/** 0x2437 */
typedef struct {
    int16_t rowEvenColumnEven : 8;
    int16_t rowOddColumnEven : 8;
} Mlx90640_eepromVdd_t;

/** 0x2438 */
typedef struct {
    uint16_t ktaScale2 : 4;
    uint16_t ktaScale1 : 4;
    uint16_t kvScale  : 4;
    uint16_t resControlCalib : 2;
    uint16_t mlxReserved : 2;
} Mlx90640_eepromKvKtaScale_t;


/** 0x2440 - 0x273F */
typedef struct {
    uint16_t outlier : 1;
    uint16_t kta : 3;
    uint16_t alphaPixel : 6;
    uint16_t offsetPixel : 6;
} Mlx90640_eepromPixelCalib_t;
static_assert (sizeof(Mlx90640_eepromPixelCalib_t) == 2, "Size is not correct");

/** 0x2412 - 0x241F */
/** 0x2422 - 0x242F */
typedef struct {
    int16_t occRow1 : 4;
    int16_t occRow2 : 4;
    int16_t occRow3 : 4;
    int16_t occRow4 : 4;
} Mlx90640_occAccRowColumn_t;
static_assert (sizeof(Mlx90640_occAccRowColumn_t) == 2, "Size is not correct");

/** 0x2440 - 0x273F */
typedef struct {
    uint16_t oscTrim;   /* also MLX reserved */
    uint16_t anaTrim;   /* also MLX reserved */
    uint16_t mlxReservedEe1;
    uint16_t configRegEe;
    uint16_t mlxReservedEe2;
    uint16_t mlxReservedEe3;
    uint16_t mlxReservedEe4;
    uint16_t mlxID1;
    uint16_t mlxID2;
    uint16_t mlxID3;
    uint16_t mlxReservedEe5;
    uint16_t mlxReservedEe6;
    uint16_t controlRegEe1;
    uint16_t controlRegEe2;
    uint16_t i2cConf;
    uint16_t i2cAddr;
    /* */
    Mlx90640_eepromPtatOccScale_t ptatOccScale; 
    int16_t pixOsAvg;
    Mlx90640_occAccRowColumn_t occRowsArray[6u];
    Mlx90640_occAccRowColumn_t occColumnArray[8u];
    Mlx90640_eepromAccScale_t accScale;
    uint16_t pixSensAvg;
    /* */
    Mlx90640_occAccRowColumn_t occRowsArray[6u];
    Mlx90640_occAccRowColumn_t occColumnArray[8u];
    /* */
    int16_t gain;
    int16_t ptat25;
    Mlx90640_eepromKPtat_t kPtat;
    Mlx90640_eepromVdd_t vdd;
    Mlx90640_eepromKvAvgRowCol_t kvAvgRowCol;
    Mlx90640_eepromIlChess_t ilChess;
    Mlx90640_eepromKvKtaScale_t
    uint16_t ktaAvg;
    uint16_t mlxReservedEe7;
    /* typedef here 8 words*/
    Mlx90640_eepromPixelCalib_t pixelCalibArray[768u];  /**< 0x2440 - 0x273F */
} Mlx90640_eeprom_t;
/* assert to check size */
#endif /* MLX90640_FP_LIB */

/* EOF */
